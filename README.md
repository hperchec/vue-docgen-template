# @hperchec/vue-docgen-template

Template for vue-docgen-cli including:

- script handlers to extract component documentation:
  - get **extends** informations
  - get **mixins** informations
  - parse **props** doc blocks with jsDoc
  - parse **methods** doc blocks with jsDoc
- doc blocks tag features (see below)
- template engine similar to [jsdoc2md/dmd](https://github.com/jsdoc2md/dmd) written with [Handlebars.js](https://handlebarsjs.com/)
- `createConfig` util to create vue-docgen-cli config

This template make markdown output friendly for [markdownlint](https://github.com/markdownlint/markdownlint) ✔

## Usage

Use the `createConfig` method in vue-docgen-cli `docgen.config.js` configuration file:

```js
const { createConfig } = require('@hperchec/vue-docgen-template')

const docgenConfig = createConfig({
  componentsRoot: 'src/components', // the folder where CLI will start searching for components.
  components: '**/[A-Z]*.vue', // the glob to define what files should be documented as components (relative to componentRoot)
  outDir: 'docs' // folder to save components docs in (relative to the current working directory)
}, {
  /* Template options */
})

module.exports = docgenConfig
```

See also [API docs](https://gitlab.com/hperchec/vue-docgen-template/-/tree/main/docs)

## Options

The `createConfig` method takes **vue-docgen-cli** config object as first parameter and options as second parameter:

- `options.helper?: String | String[]`: handlebars helpers file(s)
- `options.partial?: String | String[]`: handlebars partial file(s)
- `options.template?: String`: handlebars root template as string
- `options.jsDoc?: Object`: jsDoc options

See also [API docs](https://gitlab.com/hperchec/vue-docgen-template/-/tree/main/docs)

## Document component

### Documentation object

[Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object has now the following properties:

- `category`: component category from **@category** tag
- `deprecated`: if component is deprecated (from **@deprecated** tag)
- `isGloballyRegistered`: true if component has **@global** tag
- `see`: from **@see** tag
- `since`: from **@since** tag
- `version`: from **@version** tag

### Doc blocks

Doc blocks are now parsed by jsDoc engine as [Doclet](https://github.com/jsdoc/jsdoc/blob/0391cf70a364fa05ba6c91105600b587a9b3faa4/packages/jsdoc-doclet/lib/doclet.js#L369) objects.

Added tags:

- `@category` tag will add category to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `category` property (`Array`)

Changes:

- `@deprecated` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `deprecated` property (`Boolean | String`)
- `@global` tag will mark component as globally registered by Vue. It sets component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `isGloballyRegistered` property (`Boolean`) to `true`.
- `@see` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `see` property (`Array`)
- `@since` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `since` property (`String`)
- `@version` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `version` property (`String`)

### Props

#### PropDescriptor object

[PropDescriptor](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-inbrowser-compiler-independent-utils/src/types.ts#L71) object has now the following properties:

- `category`: prop category from **@category** tag
- `deprecated`: if prop is deprecated (from **@deprecated** tag)
- `see`: from **@see** tag
- `since`: from **@since** tag
- `values`: prop values from **@values** tag
- `version`: from **@version** tag

#### Doc blocks

Doc blocks are now parsed by jsDoc engine as [Doclet](https://github.com/jsdoc/jsdoc/blob/0391cf70a364fa05ba6c91105600b587a9b3faa4/packages/jsdoc-doclet/lib/doclet.js#L369) objects.

Added tags:

- `@category` tag will add category to [PropDescriptor](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-inbrowser-compiler-independent-utils/src/types.ts#L71) object `category` property (`Array`)

Changes:

- `@deprecated` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `deprecated` property (`Boolean | String`)
- `@see` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `see` property (`Array`)
- `@since` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `since` property (`String`)
- `@values` tag will add category to [PropDescriptor](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-inbrowser-compiler-independent-utils/src/types.ts#L71) object `values` property (`Array`)
- `@version` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `version` property (`String`)

### Methods

#### MethodDescriptor object

[MethodDescriptor](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-inbrowser-compiler-independent-utils/src/types.ts#L81) object has now the following properties:

- `category`: prop category from **@category** tag
- `deprecated`: if prop is deprecated (from **@deprecated** tag)
- `see`: from **@see** tag
- `since`: from **@since** tag
- `version`: from **@version** tag

#### Doc blocks

Doc blocks are now parsed by jsDoc engine as [Doclet](https://github.com/jsdoc/jsdoc/blob/0391cf70a364fa05ba6c91105600b587a9b3faa4/packages/jsdoc-doclet/lib/doclet.js#L369) objects.

Added tags:

- `@category` tag will add category to [MethodDescriptor](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-inbrowser-compiler-independent-utils/src/types.ts#L81) object `category` property (`Array`)

Changes:

- `@deprecated` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `deprecated` property (`Boolean | String`)
- `@see` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `see` property (`Array`)
- `@since` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `since` property (`String`)
- `@version` tag is exposed to component [Documentation](https://github.com/vue-styleguidist/vue-styleguidist/blob/03687ec3ac57ce416a9c7c4381cc70abb076f218/packages/vue-docgen-api/src/Documentation.ts#L36) object `version` property (`String`)