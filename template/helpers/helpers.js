const markdownTable = require('markdown-table')
const YAML = require('yaml')
const hbsHelpers = require('handlebars-helpers')()
const dmdHelpers = {
  helpers: require('dmd/helpers/helpers'),
  ddata: require('dmd/helpers/ddata')
}

/**
 * Build an array with arguments and returns it
 * @param  {...any} args - The value to put into array
 * @returns {Array}
 */
function arrayFrom (...args) {
  return [ ...args ]
}

/**
 * Check if a plain object is "empty" (no key defined)
 * @param {Object} obj - The object to check
 * @returns {boolean}
 */
function isObjectEmpty (obj) {
  return Object.keys(obj).length === 0
}

/**
 * Returns true if value is string
 * @param {*} value - The value to check
 * @returns {boolean}
 */
function isString (value) {
  return typeof value === 'string'
}

/**
 * replaces returns and tubes to make the input compatible with markdown
 * @param {String} input
 * @returns {String}
 */
function mdclean (input) {
	return input.replace(/\r?\n/g, '<br/>').replace(/\|/g, '\\|')
}

/**
 * Return a markdown table string using markdown-table library
 * Take same arguments as mardownTable (see: https://github.com/wooorm/markdown-table/tree/976ac8a55d5eb4b030691ee0f0d44f671191d5bc)
 * @returns {string}
 */
function mdTable (...args) {
  return markdownTable(...args)
}

/**
 * Returns component props table in markdown format
 * @param {Object[]} props - The props as received by vue-docgen (PropDescriptor[])
 */
function propsTable (props) {
  const renderDescription = (prop) => {
    return mdclean(
      (prop.description || '') +
      (prop.deprecated ? `\n**Deprecated** ${typeof prop.deprecated === 'string' ? prop.deprecated : ''}` : '') +
      (prop.category ? `\n**Category** ${prop.category.join()}` : '') +
      renderTags(prop.tags)
    )
  }
  const renderTags = (tags) => {
    if (!tags) return ''
    return tags.map((tag) => {
      return `\n\`@${tag.title}\`${tag.value ? ` ${tag.value}` : ''}`
    }).join('')
  }
  return mdTable([
    [ 'Prop name', 'Description', 'Type', 'Values', 'Default', 'Origin' ],
    ...props.map(p => {
      const name = p.name
      const description = renderDescription(p)
      const type = p.type && p.type.names ? `\`${p.type.names.join(' \\| ')}\`` : ''
      const values = p.values ? p.values.map(pv => `\`${pv}\``).join(', ') : ''
      const defaultValue = p.defaultValue ? p.defaultValue.value : ''
      const origin = p.mixin
        ? p.mixin.name
        : p.extends
          ? p.extends.name
          : ''
      return [ name, description, type, values, defaultValue, origin ]
    })
  ], {
    alignDelimiters: false
  })
}

/**
 * Returns component events table in markdown format
 * @param {Object[]} events - The events as received by vue-docgen (EventDescriptor[])
 */
function eventsTable (events) {
  const formatProperties = (properties) => {
    if (!properties) {
      return ''
    }
    return properties
      .map(property => {
        const { name, description, type } = property
        if (!type) {
          return ''
        }
        return `**${name}** \`${type.names.length ? type.names.join(', ') : ''}\` - ${description}`
      })
      .join('\n')
  }
  return mdTable([
    [ 'Event name', 'Properties', 'Description'],
    ...events.map(e => {
      const name = e.name
      const properties = formatProperties(e.properties)
      const description = e.description || ''
      return [ name, mdclean(properties), mdclean(description) ]
    })
  ], {
    alignDelimiters: false
  })
}

/**
 * Returns component slots table in markdown format
 * @param {Object[]} slots - The slots as received by vue-docgen (SlotDescriptor[])
 */
function slotsTable (slots) {
  const formatBindings = (bindings) => {
    if (!bindings) {
      return ''
    }
    return bindings
      .map(binding => {
        const { name, description, type } = binding
        if (!type) {
          return ''
        }
        return `**${name}** \`${
          type.name === 'union' && type.elements
            ? type.elements.map(({ name: insideName }) => insideName).join(', ')
            : type.name
        }\` - ${description}`
      })
      .join('\n')
  }
  return mdTable([
    [ 'Name', 'Description', 'Bindings' ],
    ...slots.map(s => {
      const name = s.name
      const description = s.description || ''
      const bindings = formatBindings(s.bindings)
      return [ name, mdclean(description), mdclean(bindings) ]
    })
  ], {
    alignDelimiters: false
  })
}

/**
 * Transform value to YAML format
 * @param {*} value - The value to stringify
 * @returns {string}
 */
function yamlStringify (value) {
  return YAML.stringify(value)
}

module.exports = {
  // handlebars-helpers
  first: hbsHelpers.first,
  join: hbsHelpers.join,
  or: hbsHelpers.or,
  // dmd: helpers
  deprecated: dmdHelpers.helpers.deprecated,
  equal: dmdHelpers.helpers.equal,
  escape: dmdHelpers.helpers.escape,
  examples: dmdHelpers.helpers.examples,
  inlineLinks: dmdHelpers.helpers.inlineLinks,
  params: dmdHelpers.helpers.params,
  parseType: dmdHelpers.helpers.parseType,
  'regexp-test': dmdHelpers.helpers['regexp-test'],
  'string-repeat': dmdHelpers.helpers['string-repeat'],
  tableHead: dmdHelpers.helpers.tableHead,
  tableHeadHtml: dmdHelpers.helpers.tableHeadHtml,
  tableRow: dmdHelpers.helpers.tableRow,
  // dmd: ddata
  link: dmdHelpers.ddata.link,
  sig: dmdHelpers.ddata.sig,
  stripNewlines: dmdHelpers.ddata.stripNewlines,
  // custom
  arrayFrom,
  isObjectEmpty,
  isString,
  mdTable,
  propsTable,
  eventsTable,
  slotsTable,
  'yaml-stringify': yamlStringify
}