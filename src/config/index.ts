import path from 'path';
import { DocgenCLIConfig } from 'vue-docgen-cli';
import readFile from '../utils/read-file';
import getFileUrls from '../utils/get-file-urls';
import getPartials from '../utils/get-partials';
import getHelpers from '../utils/get-helpers';
import toAbsolutePath from '../utils/to-absolute-path';
import createVueDocgenComponent from '../template/lib/create-component';
import { preHandlers, defaultHandlers, handlers } from '../script-handlers';

const templatePath = path.resolve(__filename, '../../../template');

// Default options
const defaultOptions = {
  template: readFile(path.join(templatePath, 'component.hbs')),
  helpers: getHelpers([ path.join(templatePath, 'helpers/helpers.js') ]),
  partials: getPartials(getFileUrls(`**/*.hbs`, { cwd: path.join(templatePath, 'partials'), absolute: true }))
}

export interface CreateConfigOptions {
  helper?: string | string[];
  partial?: string | string[];
  template?: string;
  jsDoc?: any; // JsdocOptions
};

/**
 * createConfig
 * @alias module:vueDocgenTemplate.createConfig
 * @param {DocgenCLIConfig} [userConfig] - The vue-docgen-cli config (docgen.config.js)
 * @param {CreateConfigOptions} [options = {}] - The options
 * @param {String|String[]} [options.helper] - The handlebars helper file(s). Similar to jsdoc2md option
 * @param {String|String[]} [options.partial] - The handlebars partial file(s). Similar to jsdoc2md option
 * @param {String} [options.template] - The handlebars root template as string.
 * @param {Object} [options.jsDoc] - jsdoc-api explainSync [options](https://www.npmjs.com/package/jsdoc-api#module_jsdoc-api..JsdocOptions)
 * @returns {DocgenCLIConfig}
 * @description
 * Takes docgen.config.js exported object as first argument and apply template
 * ```js
 * const { createConfig } = require('@hperchec/vue-docgen-template')
 * ```
 * @example
 * const docgenConfig = createConfig({
 *   componentsRoot: 'src/components',
 *   outDir: 'docs'
 * }, {
 *   // options
 * })
 */
export function createConfig (userConfig: DocgenCLIConfig, options: CreateConfigOptions = {}): DocgenCLIConfig {
  const createComponentOptions = {
    template: options.template || defaultOptions.template,
    helpers: {
      ...defaultOptions.helpers,
      ...getHelpers((options.helper ? Array.isArray(options.helper) ? options.helper : [ options.helper ] : []).map((file) => toAbsolutePath(file)))
    },
    partials: {
      ...defaultOptions.partials,
      ...getPartials((options.partial ? Array.isArray(options.partial) ? options.partial : [ options.partial ] : []).map((file) => toAbsolutePath(file)))
    }
  }
  // Create default component template function
  const templatesComponent = createVueDocgenComponent(createComponentOptions);
  // Return docgen config object
  const config = {
    ...userConfig,
    templates: {
      component: userConfig.templates?.component || templatesComponent,
      events: function () { return '' }, // don't need renderedUsage.events anymore
      methods: function () { return '' }, // don't need renderedUsage.methods anymore
      props: function () { return '' }, // don't need renderedUsage.props anymore
      slots: function () { return '' }, // don't need renderedUsage.slots anymore
      expose: function () { return '' }, // don't need renderedUsage.expose anymore
      defaultExample: function () { return '' }, // don't need renderedUsage.defaultExample anymore
      functionalTag: '', // don't need renderedUsage.functionalTag anymore
    },
    apiOptions: {
      scriptPreHandlers: userConfig.apiOptions?.scriptPreHandlers || preHandlers,
      addScriptHandlers: userConfig.apiOptions?.addScriptHandlers
        ? handlers.concat(userConfig.apiOptions.addScriptHandlers)
        : handlers,
      scriptHandlers: userConfig.apiOptions?.scriptHandlers || defaultHandlers,
      // Custom config field
      meta: {
        jsDoc: options.jsDoc || {}
      }
    }
  }
  return config as DocgenCLIConfig
}

export default createConfig;