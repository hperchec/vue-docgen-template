import { DocGenOptions, ParseOptions } from 'vue-docgen-api/dist/parse';

export type MetaDocGenOption = {
  jsDoc?: any; // https://www.npmjs.com/package/jsdoc-api#module_jsdoc-api..JsdocOptions
}

export interface ExtendedDocGenOptions extends DocGenOptions {
  meta?: MetaDocGenOption
}

export interface ExtendedParseOptions extends ParseOptions {
  meta?: MetaDocGenOption
}