/**
 * @module vueDocgenTemplate
 */

export * from './script-handlers';

export { createComponent as createTemplateComponent } from './template/lib/create-component';

export * from './config';

export { createConfig as default } from './config';