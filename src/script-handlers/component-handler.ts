import * as bt from '@babel/types';
import { NodePath } from 'ast-types/lib/node-path';
import Documentation from 'vue-docgen-api/dist/Documentation';
import getDocblock from 'vue-docgen-api/dist/utils/getDocblock';
import getProperties from 'vue-docgen-api/dist/script-handlers/utils/getProperties';
import { Doclet } from '@jsdoc/doclet';
import jsDoc from 'jsdoc-api';
import { ExtendedParseOptions }  from '../interfaces';

/**
 * @ignore
 * Override default script handler componentHandler to parse doc block with jsDoc
 */
export default async function componentHandler (
  documentation: Documentation,
  compDef: NodePath,
  ast: bt.File,
  opt: ExtendedParseOptions
): Promise<void> {
  // deal with functional flag
  if (bt.isObjectExpression(compDef.node)) {
    const functionalPath = getProperties(compDef, 'functional')

    if (functionalPath.length) {
      const functionalValue = functionalPath[0].get('value').node
      if (bt.isBooleanLiteral(functionalValue)) {
        documentation.set('functional', functionalValue.value)
      }
    }
  }

  let componentCommentedPath = compDef.parentPath
  // in case of Vue.extend() structure
  if (bt.isCallExpression(componentCommentedPath.node)) {
    // look for leading comments in the parent structures
    let i = 5
    while (
      i-- &&
      !componentCommentedPath.get('leadingComments').value &&
      componentCommentedPath.parentPath.node.type !== 'Program'
    ) {
      componentCommentedPath = componentCommentedPath.parentPath
    }
  } else if (bt.isVariableDeclarator(componentCommentedPath.node)) {
    componentCommentedPath = componentCommentedPath.parentPath.parentPath
    if (componentCommentedPath.parentPath.node.type !== 'Program') {
      componentCommentedPath = componentCommentedPath.parentPath
    }
  } else if (bt.isDeclaration(componentCommentedPath.node)) {
    const classDeclaration = componentCommentedPath.get('declaration')
    if (bt.isClassDeclaration(classDeclaration.node)) {
      componentCommentedPath = classDeclaration
    }
  }

  const docBlock = getDocblock(componentCommentedPath)

  // if no prop return
  if (!docBlock || !docBlock.length) {
    return Promise.resolve()
  }

  const docBlockSource = '/**\n * ' +
    docBlock.replace(/\n/g, '\n * ') +
    '\n */\n' +
    'var component = {}'

  // Get doclet from comment source
  const doclet: Doclet = jsDoc.explainSync({
    ...opt?.meta?.jsDoc,
    source: docBlockSource
  })[0]
  // Get unknown tags
  const jsDocTags = doclet.tags ? doclet.tags : []

  // Description
  documentation.set('description', doclet.description)
  // Globally registered ?
  documentation.set('isGloballyRegistered', doclet.scope === 'global')
  // Deprecated
  if (doclet.deprecated) documentation.set('deprecated', doclet.deprecated)
  // Author
  if (doclet.author) documentation.set('author', doclet.author)
  // Version
  if (doclet.version) documentation.set('version', doclet.version)
  // Since
  if (doclet.since) documentation.set('since', doclet.since)
  // Summary
  if (doclet.summary) documentation.set('summary', doclet.summary)
  // See
  if (doclet.see) documentation.set('see', doclet.see)
  // Examples
  if (doclet.examples) documentation.set('examples', doclet.examples)

  if (jsDocTags) {
    // Find custom tag @displayName
    const displayNamesTags = jsDocTags.filter(t => t.title === 'displayName')
    if (displayNamesTags.length) {
      const displayName = displayNamesTags[0]
      documentation.set('displayName', displayName.value)
    }
    // Find @category tags
    const categoryTags = jsDocTags.filter(t => t.title === 'category')
    if (categoryTags.length) {
      documentation.set('category', categoryTags.map((t) => t.value))
    }

    documentation.set('tags', jsDocTags.reduce((tags, t) => {
      if (t.title !== 'displayName' && t.title !== 'category') {
        tags.push(t)
      }
      return tags
    }, []))
  } else {
    documentation.set('tags', [])
  }
  return Promise.resolve()
}