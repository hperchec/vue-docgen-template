import * as bt from '@babel/types';
import Documentation from 'vue-docgen-api/dist/Documentation';
import { NodePath } from 'ast-types/lib/node-path';
import resolveRequired from 'vue-docgen-api/dist/utils/resolveRequired'
import resolveLocal from 'vue-docgen-api/dist/utils/resolveLocal'
import { addDefaultAndExecuteHandlers } from 'vue-docgen-api/dist/parse-script'
import { ExtendedParseOptions }  from '../interfaces';
import documentRequiredComponents from '../utils/document-required-components';

/**
 * @ignore
 * Look in the extends section of a component.
 * Parse the file extends point to.
 * Add the necessary info to the current doc object.
 * Must be run first as extends do not override components.
 * > Expose directly extends in documentation 'extends' property as an object
 */
export default async function extendsHandler (
  documentation: Documentation,
  compDef: NodePath,
  ast: bt.File,
  opt: ExtendedParseOptions
) {
  const extendsVariableName = getExtendsVariableName(compDef)

  // if there is no extends or extends is a direct require
  if (!extendsVariableName) {
    return
  }

  const variablesResolvedToCurrentFile = resolveLocal(ast, [extendsVariableName])

  if (variablesResolvedToCurrentFile.get(extendsVariableName)) {
    await addDefaultAndExecuteHandlers(
      variablesResolvedToCurrentFile,
      ast,
      {
        ...opt,
        nameFilter: [extendsVariableName]
      },
      documentation
    )
  } else {
    // get all require / import statements
    const extendsFilePath = resolveRequired(ast, [extendsVariableName])

    // get each doc for each mixin using parse
    await documentRequiredComponents(documentation, extendsFilePath, 'extends', opt)
  }
}

/**
 * @ignore
 */
function getExtendsVariableName(compDef: NodePath): string | undefined {
  const extendsVariable: NodePath | undefined =
    compDef &&
    bt.isClassDeclaration(compDef.node) &&
    compDef.node.superClass &&
    bt.isIdentifier(compDef.node.superClass)
      ? (compDef.get('superClass') as NodePath<bt.Identifier>)
      : getExtendsVariableNameFromCompDef(compDef)

  if (extendsVariable) {
    const extendsValue = bt.isProperty(extendsVariable.node)
      ? extendsVariable.node.value
      : extendsVariable.node
    return extendsValue && bt.isIdentifier(extendsValue) ? extendsValue.name : undefined
  }
  return undefined
}

/**
 * @ignore
 */
function getExtendsVariableNameFromCompDef(compDef: NodePath): NodePath | undefined {
  if (!compDef) {
    return undefined
  }
  const compDefProperties = compDef.get('properties')
  const pathExtends = compDefProperties.value
    ? compDefProperties.filter(
        (p: NodePath<bt.Property>) => bt.isIdentifier(p.node.key) && p.node.key.name === 'extends'
      )
    : []
  return pathExtends.length ? pathExtends[0] : undefined
}
