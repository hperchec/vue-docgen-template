import * as bt from '@babel/types';
import Documentation, {
  BlockTag,
  DocBlockTags,
  MethodDescriptor,
  Tag
} from 'vue-docgen-api/dist/Documentation';
import getDocblock from 'vue-docgen-api/dist/utils/getDocblock';
import getDoclets from 'vue-docgen-api/dist/utils/getDoclets';
import getTypeFromAnnotation from 'vue-docgen-api/dist/utils/getTypeFromAnnotation';
import getProperties from 'vue-docgen-api/dist/script-handlers/utils/getProperties';
import jsDoc from 'jsdoc-api';
import { ExtendedParseOptions }  from '../interfaces';
import { NodePath } from 'ast-types/lib/node-path';
import { Doclet } from '@jsdoc/doclet';

/**
 * TypeScript equivalent of https://github.com/jsdoc/jsdoc/blob/0391cf70a364fa05ba6c91105600b587a9b3faa4/packages/jsdoc-doclet/lib/schema.js#L171
 */
export interface JsDocDocletParam {
  defaultvalue?: any;
  description?: string;
  name: string;
  nullable?: boolean;
  optional?: boolean;
  type: {
    names: string[];
    // type parser output
    parsedType: Object;
  };
  // can this parameter be repeated?
  variable?: boolean;
}

/**
 * @ignore
 * Override default script handler methodHandler to parse doc block with jsDoc
 */
export default async function methodHandler (
  documentation: Documentation,
  compDef: NodePath,
  ast: bt.File,
  opt: ExtendedParseOptions
): Promise<void> {
  if (bt.isObjectExpression(compDef.node)) {
    const exposePath = getProperties(compDef, 'expose')
    const exposeArray =
      exposePath[0]?.get('value', 'elements').map((el: NodePath) => el.value.value) || []
    const methodsPath = getProperties(compDef, 'methods')

    // if no method return
    if (!methodsPath.length) {
      return Promise.resolve()
    }

    const methodsObject = methodsPath[0].get('value')
    if (bt.isObjectExpression(methodsObject.node)) {
      // Loop on methods
      methodsObject.get('properties').each((p: NodePath) => {
        let methodName = '<anonymous>'
        if (bt.isObjectProperty(p.node) && bt.isIdentifier(p.node.key)) {
          const val = p.get('value')
          methodName = p.node.key.name
          if (!Array.isArray(val)) {
            p = val
          }
        }
        // Get method name
        methodName = bt.isObjectMethod(p.node) && bt.isIdentifier(p.node.key) ? p.node.key.name : methodName
        // Get doc block content as raw source
        const docBlock = getDocblock(bt.isObjectMethod(p.node) ? p : p.parentPath) || ''
        const docBlockSource = '/**\n * ' +
          docBlock.replace(/\n/g, '\n * ') +
          '\n */\n' +
          `function ${methodName} () {}`
        // Get doclet from comment source
        const doclet: Doclet = jsDoc.explainSync({
          ...opt?.meta?.jsDoc,
          source: docBlockSource
        })[0]
        // Set doclet method name
        doclet.name = methodName
        // Also extract doclet vue vue-docgen lib to find @private tag or @access tag.
        // jsdoc treat as public by default, so parse manually docBlock
        // to find @public tag or @access
        const _doclet: DocBlockTags = docBlock ? getDoclets(docBlock) : { description: '', tags: [] }
        const jsDocTags: BlockTag[] = _doclet.tags ? _doclet.tags : []
        // Considering methods are private by default.
        const __config_private__ = true;
        if (__config_private__) {
          // Ignore if marked as private by jsdoc
          if (doclet.access === 'private') return
          // Ignore the method if there is no public tag extracted by vue-docgen getDoclets
          if (
            (!jsDocTags.some((t) => {
              const _t = t as Tag
              return (_t.title === 'access' && _t.content && _t.content === 'public')
            }) &&
            !exposeArray.includes(methodName))
          ) {
            return
          }
        }
        // Create method descriptor object
        const methodDescriptor = documentation.getMethodDescriptor(methodName)
        // Set method descriptor
        setMethodDescriptor(methodDescriptor, p as NodePath<bt.Function>, doclet)
      })
    }
  }
  return Promise.resolve()
};

/**
 * @ignore
 */
export function setMethodDescriptor (
  methodDescriptor: MethodDescriptor,
  method: NodePath<bt.Function>,
  doclet: Doclet
) {
  // First, set method descriptor with doclet object
  for (const key in doclet) {
    let value
    if (key === 'tags') {
      // Find @category tags
      const tags = doclet[key]
      const categoryTags = tags.filter((t) => t.title === 'category')
      if (categoryTags.length) {
        methodDescriptor.category = categoryTags.map((t) => t.value)
      }
      value = tags.filter((t) => t.title !== 'category')
    } else {
      value = doclet[key]
    }
    methodDescriptor[key] = value
  }
  // Override kind property for dmd helpers
  methodDescriptor.kind = 'function'
  // Override params
  methodDescriptor.params = describeParams(
    method,
    doclet.params
  )
  // Override returns
  methodDescriptor.returns = describeReturns(
    method,
    doclet.returns
  )
  return methodDescriptor
}

/**
 * @ignore
 */
function describeParams(
  methodPath: NodePath<bt.Function>,
  jsDocParams: JsDocDocletParam[] // jsdoc Doclet params property
) {
  // if there is no parameter no need to parse them
  const fExp = methodPath.node
  if (!fExp.params || !jsDocParams || (!fExp.params.length && !jsDocParams.length)) {
    return
  }

  const params: JsDocDocletParam[] = []
  fExp.params.forEach((par: any, i) => {
    let name: string
    if (bt.isIdentifier(par)) {
      // simple params
      name = par.name
    } else if (bt.isIdentifier(par.left)) {
      // es6 default params
      name = par.left.name
    } else if (bt.isRestElement(par)){
      // 'rest' param (ex: ...args)
      name = (par.argument as bt.Identifier).name
    } else {
      // unrecognized pattern
      return
    }

    let jsDocParam = jsDocParams.find(param => param.name === name)

    // if tag is not namely described try finding it by its order
    if (!jsDocParam) {
      if (jsDocParams[i] && !jsDocParams[i].name) {
        jsDocParam = jsDocParams[i]
      }
    }

    const param = jsDocParam || { name } as JsDocDocletParam

    // if TypeScript typed parameter
    if (!param.type && par.typeAnnotation) {
      const type = getTypeFromAnnotation(par.typeAnnotation)
      if (type) {
        // type is ParamType
        // TODO: transform ParamType to jsDoc Doclet param item object
        // param.type = type
      }
    }

    params.push(param)
  })

  // in case the arguments are abstracted (using the arguments keyword)
  if (!params.length) {
    jsDocParams.forEach(doc => {
      params.push(doc)
    })
  }

  if (params.length) {
    return params as any[]
  } else {
    return undefined
  }
}

/**
 * @ignore
 */
function describeReturns(
  methodPath: NodePath<bt.Function>,
  jsDocReturns: JsDocDocletParam[] // Same type as Doclet params
) {
  // Get first array item
  const returns = jsDocReturns ? jsDocReturns[0] as any : undefined
  // If returns undefined or not typed, check TypeScript annotation
  if (!returns || !returns.type) {
    const methodNode = methodPath.node
    if (methodNode.returnType) {
      const type = getTypeFromAnnotation(methodNode.returnType)
      if (type) {
        // type is ParamType
        // TODO: transform ParamType to jsDoc Doclet param item object
        // methodDescriptor.returns = methodDescriptor.returns || {}
        // methodDescriptor.returns.type = type
      }
    }
  }
  return returns
}
