import * as bt from '@babel/types';
import Documentation from 'vue-docgen-api/dist/Documentation';
import { NodePath } from 'ast-types/lib/node-path';
import resolveRequired from 'vue-docgen-api/dist/utils/resolveRequired';
import resolveLocal from 'vue-docgen-api/dist/utils/resolveLocal';
import getProperties from 'vue-docgen-api/dist/script-handlers/utils/getProperties';
import { addDefaultAndExecuteHandlers } from 'vue-docgen-api/dist/parse-script';
import { ExtendedParseOptions }  from '../interfaces';
import documentRequiredComponents from '../utils/document-required-components';

/**
 * @ignore
 * Look in the mixin section of a component.
 * Parse the file mixins point to.
 * Add the necessary info to the current doc object.
 * Must be run first as mixins do not override components.
 * > Expose directly mixins in documentation 'mixins' property as an object of mixins
 */
export default async function mixinsHandler(
  documentation: Documentation,
  componentDefinition: NodePath,
  astPath: bt.File,
  opt: ExtendedParseOptions
) {
  // filter only mixins
  const mixinVariableNames = getMixinsVariableNames(componentDefinition)

  if (!mixinVariableNames || !mixinVariableNames.length) {
    return
  }

  const variablesResolvedToCurrentFile = resolveLocal(astPath, mixinVariableNames)

  // get require / import statements for mixins
  const mixinVarToFilePath = resolveRequired(astPath, mixinVariableNames)

  await mixinVariableNames.reduce(async (_, varName) => {
    await _
    if (variablesResolvedToCurrentFile.get(varName)) {
      await addDefaultAndExecuteHandlers(
        variablesResolvedToCurrentFile,
        astPath,
        {
          ...opt,
          nameFilter: [varName]
        },
        documentation
      )
    } else {
      // get each doc for each mixin using parse
      await documentRequiredComponents(documentation, mixinVarToFilePath, 'mixin', {
        ...opt,
        nameFilter: [varName]
      })
    }

    return
  }, Promise.resolve())
}

/**
 * @ignore
 */
function getMixinsVariableNames(compDef: NodePath): string[] {
  const varNames: string[] = []
  if (bt.isObjectExpression(compDef.node)) {
    const mixinProp = getProperties(compDef, 'mixins')

    const mixinPath = mixinProp.length ? (mixinProp[0] as NodePath<bt.Property>) : undefined

    if (mixinPath) {
      const mixinPropertyValue =
        mixinPath.node.value && bt.isArrayExpression(mixinPath.node.value)
          ? mixinPath.node.value.elements
          : []
      mixinPropertyValue.forEach((e: bt.Node | null) => {
        if (!e) {
          return
        }
        if (bt.isCallExpression(e)) {
          e = e.callee
        }
        if (bt.isIdentifier(e)) {
          varNames.push(e.name)
        }
      })
    }
  } else if (
    bt.isClassDeclaration(compDef.node) &&
    compDef.node.superClass &&
    bt.isCallExpression(compDef.node.superClass) &&
    bt.isIdentifier(compDef.node.superClass.callee) &&
    compDef.node.superClass.callee.name.toLowerCase() === 'mixins'
  ) {
    return compDef.node.superClass.arguments.reduce((acc: string[], a) => {
      if (bt.isIdentifier(a)) {
        acc.push(a.name)
      }
      return acc
    }, [])
  }
  return varNames
}
