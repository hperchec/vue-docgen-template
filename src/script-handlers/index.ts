import vueDocgenDefaultScriptHandlers from 'vue-docgen-api/dist/script-handlers';
import extendsHandler from './extends-handler';
import mixinsHandler from './mixins-handler';
import componentHandler from './component-handler';
import methodHandler from './method-handler';
import propHandler from './prop-handler';
import { Handler } from 'vue-docgen-api/dist/parse-script';

/**
 * @alias module:vueDocgenTemplate.preHandlers
 * @type {Handler[]}
 * @description
 * Overrides default vue-docgen-api script pre-handlers.
 *
 * We add to the `documentation` object the following properties:
 *
 * - `extends`: the component parents
 * - `mixins`: the mixins used by the component
 *
 * ```js
 * const { preHandlers } = require('@hperchec/vue-docgen-template')
 * ```
 */
export const preHandlers: Handler[] = [
  // have to be first if they can be overridden
  extendsHandler,
  // have to be second as they can be overridden too
  mixinsHandler
];

/**
 * @alias module:vueDocgenTemplate.defaultHandlers
 * @type {Handler[]}
 * @description
 * Overrides default vue-docgen-api script handlers.
 *
 * The following are overridden:
 * - `methodHandler`: it now parses method comment with jsDoc engine (Doclet instance)
 * - `propHandler`: it now parses prop comment with jsDoc engine (Doclet instance)
 *
 * ```js
 * const { defaultHandlers } = require('@hperchec/vue-docgen-template')
 * ```
 */
export const defaultHandlers: Handler[] = vueDocgenDefaultScriptHandlers.map((h) => {
  switch (h.name) {
    case 'componentHandler':
      return componentHandler
      break;
    case 'methodHandler':
      return methodHandler
      break;
    case 'propHandler':
      return propHandler
      break;
    default:
      return h
      break;
  }
});

/**
 * @alias module:vueDocgenTemplate.handlers
 * @type {Handler[]}
 * @description
 * Custom scripts to add to handlers queue via `apiOptions.addScriptHandlers`
 *
 * ```js
 * const { handlers } = require('@hperchec/vue-docgen-template')
 * ```
 */
export const handlers: Handler[] = [];

export default {
  preHandlers,
  defaultHandlers,
  handlers
};