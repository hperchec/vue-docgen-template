/**
 * @ignore
 * Returns an object that contains handlebars helpers.
 * @param {string[]} fileUrls - The file urls to parse
 * @returns {Object}
 */
export const getHelpers = (fileUrls: string[]): Object => {
  const helpers = {};
  for (const file of fileUrls) {
    const obj = require(file);
    for (const key of Object.keys(obj)) {
      helpers[key] = obj[key];
    }
  }
  return helpers
};

export default getHelpers;
