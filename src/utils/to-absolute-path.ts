import { isAbsolute, posix } from 'path';

/**
 * @ignore
 * Returns an absolute path (POSIX format)
 * @param {string} path
 * @param {string?} root - The root path (default: `process.cwd()`). If path argument is relative, it will be resolved with this value.
 * @returns {string} Absolute path
 */
export const toAbsolutePath = (path: string, root?: string): string => {
  return isAbsolute(path)
    ? path
    : posix.join(root || process.cwd(), path)
};

export default toAbsolutePath;
