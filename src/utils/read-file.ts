import { readFileSync } from 'fs';

/**
 * @ignore
 * Read file sync
 * @param {string} path
 * @returns {String} File content
 */
export const readFile = (path: string, options: Object = {}): string => readFileSync(path, {
  encoding: 'utf8',
  ...options
});

export default readFile;
