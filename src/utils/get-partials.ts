import getFilename from './get-filename';
import readFile from './read-file';

/**
 * @ignore
 * Returns an object that contains handlebars partials. Property name is the name of the partial, extracted from filename.
 * @param {string[]} fileUrls - The file urls to parse
 * @returns {Object}
 */
export const getPartials = (fileUrls: string[]): Object => {
  const partials = {};
  for (const file of fileUrls) {
    const name = getFilename(file);
    partials[name] = readFile(file);
  }
  return partials
};

export default getPartials;
