import { globSync, GlobOptions } from 'glob';

/**
 * @ignore
 * Get file urls that match glob (see: https://github.com/isaacs/node-glob)
 * @param {string} pattern - Same as node-glob pattern argument
 * @param {GlobOptions} [options] - Same as node-glob options argument
 * @returns {string[]} filenames
 */
export const getFileUrls = (pattern: string, options: GlobOptions): string[] => {
  let files = globSync(pattern, options);
  return files as string[];
};

export default getFileUrls;
