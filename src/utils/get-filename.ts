import path from 'path';

/**
 * @ignore
 * Get filename without extension
 * @param {string} path
 * @returns {string} filename
 */
export const getFilename = (_path: string) => {
  if (!_path || (_path && typeof _path !== 'string')) return '';

  // const splitted = path.split('/').pop() ?? path;

  // return splitted.substring(0, splitted.lastIndexOf('.'));

  return path.basename(_path, path.extname(_path))
};

export default getFilename;
