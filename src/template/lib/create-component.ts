import Handlebars from 'handlebars';

import { ComponentDoc } from 'vue-docgen-api';
import { Templates, RenderedUsage, SafeDocgenCLIConfig, ContentAndDependencies } from 'vue-docgen-cli/lib/config';
import { SubTemplateOptions } from 'vue-docgen-cli/lib/compileTemplates';
import DmdOptions from 'dmd/lib/dmd-options'

export interface CreateComponentOptions {
  template?: string,
  helpers?: Object,
  partials?: Object
}

/**
 * Create vue-docgen component template function
 * @alias module:vueDocgenTemplate.createTemplateComponent
 * @param {CreateComponentOptions} options - The options
 * @param {string} [options.template] - Component root template string
 * @param {Object} [options.partials] - Partials object
 * @param {Object} [options.helpers] - Helpers object
 * @return {Function} Returns the component template following the
 * vue-docgen-cli `apiOptions.templates.component` configuration option schema
 * ```js
 * const { createTemplateComponent } = require('@hperchec/vue-docgen-template')
 * ```
 */
export const createComponent = (
  options: CreateComponentOptions
): Templates['component'] => {
  
  // Build function
  const componentTemplate = (
    renderedUsage: RenderedUsage,
    doc: ComponentDoc,
    config: SafeDocgenCLIConfig,
    fileName: string,
    requiresMd: ContentAndDependencies[],
    { isSubComponent, hasSubComponents }: SubTemplateOptions
  ): string => {
    doc.tags = doc.tags || {}
    doc.frontMatter = doc.frontMatter || {}

    if (!config.outFile && doc.tags.deprecated) {
      // to avoid having the squiggles in the left menu for deprecated items
      // use the frontmatter feature of vuepress
      doc.frontMatter.title = doc.displayName
    }

    if (hasSubComponents) {
      // show more than one level on subcomponents
      doc.frontMatter.sidebarDepth = 2
    }

    // Register handlebars custom helpers
    for (const name in options.helpers) {
      Handlebars.registerHelper(name, options.helpers[name])
    }

    // Register partials
    for (const name in options.partials) {
      Handlebars.registerPartial(name, options.partials[name])
    }

    // Compile template
    const template = Handlebars.compile(options.template)

    // The following are now obsolete:
    // - renderedUsage.props
    // - renderedUsage.methods
    // - renderedUsage.events
    // - renderedUsage.slots

    // Pass entire documentation object as template data
    // Custom data can be set through addScriptHandlers
    return template({
      functionalTag: renderedUsage.functionalTag,
      ...doc,
      isSubComponent,
      hasSubComponents,
      // Need to add root options for dmd helpers
      options: new DmdOptions(),
      // vue-docgen parses @requires tags and calls recursively this component method
      // The result is an array of ContentAndDependencies
      requiresMd: requiresMd
    })
  }

  return componentTemplate
}

export default createComponent