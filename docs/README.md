<a name="module_vueDocgenTemplate"></a>

## vueDocgenTemplate

* [vueDocgenTemplate](#module_vueDocgenTemplate)
    * [.preHandlers](#module_vueDocgenTemplate.preHandlers) : <code>Array.&lt;Handler&gt;</code>
    * [.defaultHandlers](#module_vueDocgenTemplate.defaultHandlers) : <code>Array.&lt;Handler&gt;</code>
    * [.handlers](#module_vueDocgenTemplate.handlers) : <code>Array.&lt;Handler&gt;</code>
    * [.createConfig([userConfig], [options])](#module_vueDocgenTemplate.createConfig) ⇒ <code>DocgenCLIConfig</code>
    * [.createTemplateComponent(options)](#module_vueDocgenTemplate.createTemplateComponent) ⇒ <code>function</code>

<a name="module_vueDocgenTemplate.preHandlers"></a>

### vueDocgenTemplate.preHandlers : <code>Array.&lt;Handler&gt;</code>
Overrides default vue-docgen-api script pre-handlers.We add to the `documentation` object the following properties:- `extends`: the component parents- `mixins`: the mixins used by the component```jsconst { preHandlers } = require('@hperchec/vue-docgen-template')```

**Kind**: static property of [<code>vueDocgenTemplate</code>](#module_vueDocgenTemplate)  
<a name="module_vueDocgenTemplate.defaultHandlers"></a>

### vueDocgenTemplate.defaultHandlers : <code>Array.&lt;Handler&gt;</code>
Overrides default vue-docgen-api script handlers.The following are overridden:- `methodHandler`: it now parses method comment with jsDoc engine (Doclet instance)- `propHandler`: it now parses prop comment with jsDoc engine (Doclet instance)```jsconst { defaultHandlers } = require('@hperchec/vue-docgen-template')```

**Kind**: static property of [<code>vueDocgenTemplate</code>](#module_vueDocgenTemplate)  
<a name="module_vueDocgenTemplate.handlers"></a>

### vueDocgenTemplate.handlers : <code>Array.&lt;Handler&gt;</code>
Custom scripts to add to handlers queue via `apiOptions.addScriptHandlers````jsconst { handlers } = require('@hperchec/vue-docgen-template')```

**Kind**: static property of [<code>vueDocgenTemplate</code>](#module_vueDocgenTemplate)  
<a name="module_vueDocgenTemplate.createConfig"></a>

### vueDocgenTemplate.createConfig([userConfig], [options]) ⇒ <code>DocgenCLIConfig</code>
Takes docgen.config.js exported object as first argument and apply template```jsconst { createConfig } = require('@hperchec/vue-docgen-template')```

**Kind**: static method of [<code>vueDocgenTemplate</code>](#module_vueDocgenTemplate)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [userConfig] | <code>DocgenCLIConfig</code> |  | The vue-docgen-cli config (docgen.config.js) |
| [options] | <code>CreateConfigOptions</code> | <code>{}</code> | The options |
| [options.helper] | <code>String</code> \| <code>Array.&lt;String&gt;</code> |  | The handlebars helper file(s). Similar to jsdoc2md option |
| [options.partial] | <code>String</code> \| <code>Array.&lt;String&gt;</code> |  | The handlebars partial file(s). Similar to jsdoc2md option |
| [options.template] | <code>String</code> |  | The handlebars root template as string. |
| [options.jsDoc] | <code>Object</code> |  | jsdoc-api explainSync [options](https://www.npmjs.com/package/jsdoc-api#module_jsdoc-api..JsdocOptions) |

**Example**  
```js
const docgenConfig = createConfig({  componentsRoot: 'src/components',  outDir: 'docs'}, {  // options})
```
<a name="module_vueDocgenTemplate.createTemplateComponent"></a>

### vueDocgenTemplate.createTemplateComponent(options) ⇒ <code>function</code>
Create vue-docgen component template function

**Kind**: static method of [<code>vueDocgenTemplate</code>](#module_vueDocgenTemplate)  
**Returns**: <code>function</code> - Returns the component template following thevue-docgen-cli `apiOptions.templates.component` configuration option schema```jsconst { createTemplateComponent } = require('@hperchec/vue-docgen-template')```  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>CreateComponentOptions</code> | The options |
| [options.template] | <code>string</code> | Component root template string |
| [options.partials] | <code>Object</code> | Partials object |
| [options.helpers] | <code>Object</code> | Helpers object |

