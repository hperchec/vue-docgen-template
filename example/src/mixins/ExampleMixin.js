const ExampleMixin = {
  props: {
    /**
     * Example prop from ExampleMixin.
     * This is
     * a multiline
     * description.
     */
    exampleProp: {
      type: [String, Boolean],
      default: 'example'
    }
  },
  methods: {
    /**
     * Example method from ExampleMixin.
     * @public
     * @param {String} param1 - The first parameter
     * @param {Object} [param2 = {}] - The second parameter
     * @returns {Boolean}
     */
    exampleMethod (param1, param2 = {}) {
      return true
    }
  }
}

export default ExampleMixin
