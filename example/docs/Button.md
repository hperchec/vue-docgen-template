# Button

**Deprecated** Deprecated since the last version

Button component

> This component is globally registered by Vue

**Category**: Common component

**Version**: 1.0.0

**Authors**:

- Hervé Perchec <herve.perchec@gmail.com>

**Mixins**:

- ExampleMixin: `..\mixins\ExampleMixin.js`

## Props

| Prop name   | Description                                                                            | Type                | Values                     | Default   | Origin       |
| ----------- | -------------------------------------------------------------------------------------- | ------------------- | -------------------------- | --------- | ------------ |
| exampleProp | Example prop from ExampleMixin.<br/>This is<br/>a multiline<br/>description.           | `string \| boolean` |                            | 'example' | ExampleMixin |
| size        | The button size. Can be 'small', 'medium' or 'large'<br/>**Category** Awesome category | `string`            | `small`, `medium`, `large` | 'small'   |              |

## Methods

### exampleMethod(param1, [param2]) ⇒ `Boolean`

Example method from ExampleMixin.

**Returns**: `Boolean`

**Params**:

| Param    | Type     | Default | Description          |
| -------- | -------- | ------- | -------------------- |
| param1   | `String` |         | The first parameter  |
| [param2] | `Object` | `{}`    | The second parameter |

### ~~deprecatedMethod(param1, ...args) ⇒ `void`~~

**Deprecated**

Deprecated public documented method

**Returns**: `void`

**Category**: Public methods

**See**:

- [examplelink.html](examplelink.html)

**Params**:

| Param   | Type     | Description       |
| ------- | -------- | ----------------- |
| param1  | `String` | First parameter   |
| ...args | `*`      | Rest of arguments |

## Events

| Event name   | Properties                                                                | Description        |
| ------------ | ------------------------------------------------------------------------- | ------------------ |
| custom-click | **payload** `Object` - The payload<br/>**options** `Object` - The options | Custom click event |

## Slots

| Name    | Description  | Bindings                                                       |
| ------- | ------------ | -------------------------------------------------------------- |
| default | Default slot | **defaultClickHandler** `Function` - The default click handler |
